json.array!(@products) do |product|
  json.extract! product, :id, :producttype, :pictures, :brief, :description, :price
  json.url product_url(product, format: :json)
end
