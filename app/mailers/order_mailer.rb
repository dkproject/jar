# coding: utf-8
class OrderMailer < ApplicationMailer
default from: 'notifications@example.com'
  def order_email(order)
    @order = order
    @mymail = "lynx93@mail.ru"
    mail(to: @mymail, subject: 'Новая заявка')
  end
end