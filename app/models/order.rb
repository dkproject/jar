# encoding: UTF-8
class Order < ActiveRecord::Base
	validates :name, presence: true,
                    length: { in: 2..40 }
    validates :surname, presence: true,
                    length: { in: 2..50 }      
    validates :phone, presence: true,
                    length: { in: 5..15 },
                    :format => { :with => /[0-9]/, :message => "Only numbers"}  
end
