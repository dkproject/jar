class AdminsController < ApplicationController
  def index 

  end
  def create
    @adminname = params.require(:admin).permit(:name)[:name]
    @adminpass = Admin.find_by(name: @adminname)[:password]
    if params.require(:admin).permit(:password)[:password] == @adminpass
      session[:admin] = true
      session[:password] = @adminpass
      flash[:notice] = "Successful Login" #Вы авторизированы
      redirect_to root_path
      else
        flash[:notice] = "Login has failed - check your name and password" #Неверный логин или пароль
        redirect_to login_path
      end
  end
  def destroy
    reset_session
    flash[:notice] = "You've logged out" #Вы вышли из системы
    redirect_to login_path
  end
end
