module ApplicationHelper
	def asset_exists?(path)
		pathname = Rails.application.assets.resolve(path)
		return !!pathname # double-bang turns String into boolean
	rescue Sprockets::FileNotFound
		return false
end 
def title(page_title)
  content_for(:title) { page_title }
end
end
