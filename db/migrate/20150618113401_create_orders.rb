class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :name
      t.string :surname
      t.string :phone
      t.string :email
      t.text :comment

      t.timestamps null: false
    end
  end
end
