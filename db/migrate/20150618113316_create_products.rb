class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.integer :producttype
      t.string :pictures
      t.text :brief
      t.text :description
      t.float :price

      t.timestamps null: false
    end
  end
end
